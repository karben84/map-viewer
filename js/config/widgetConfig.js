define([
    "dojo/i18n!config/nls/local",
], function (i18n) {

    return {
        menus: [{
                title: i18n.exampleTitle,
                type: 'simple',
                icon: '',
                widget: {
                    title: i18n.exampleTitle,
                    icon: '<i class="fa fa-clone" aria-hidden="true"></i>',
                    path: 'app/widgets/example/example'
                }
            },
            {
                title: i18n.drawTitleMenu,
                type: 'dorpdown',
                icon: '',
                submenus: [                  
                    {
                        title: i18n.drawTitle,
                        icon: '',
                        widget: {
                            title: i18n.drawTitle,
                            icon: '<i class="ti-pencil"></i>',
                            path: 'app/widgets/draw/draw'
                        }
                    }
                ]
            }
        ]

    }

});