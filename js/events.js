define([], function () {

    return {
        EventsList: [],
        initEvent: function () {
            this.map.setInfoWindowOnClick(true);
        },
        addEvent: function (EventName,event) {
            this.EventsList[EventName] = event;
        },
        clearAllEvents: function () {
            this.map.setInfoWindowOnClick(false);
            if (this.EventsList["toolbar"]){
                this.EventsList["toolbar"].deactivate();
            }
            if (this.EventsList["toolbar2"]){
                this.EventsList["toolbar2"].deactivate();
            }
            if (this.EventsList["mapClick"]){
                this.EventsList["mapClick"].remove();
            }
            if (this.EventsList["layerClick"]){
                this.EventsList["layerClick"].remove();
            }
        }

    }


});