define({
    root: {
        logoName: "Map Viewer",
        mr_ml: "mr",
        help: "Aide",
        logout: "Se déconnecter",
        notification: "Notifications",
        viewAllNotification: "Voir toutes les notifications",
        language: "عربي",
        datalanguage: 'data-language =ar',
        notification_title: "Titre de notification",
        notificationTitle:"Lorem ipsum dolor sit amet consectetur, adipisicing elit"
    },
    "ar": true
});