define([
        "dojo/_base/declare",
        "dijit/_WidgetBase",
        "dijit/_TemplatedMixin",
        "dojo/text!app/widgets/example/example.html",        
        "dojo/i18n!app/widgets/example/nls/local",
        "esri/layers/FeatureLayer",
        "esri/tasks/query"

    ],
    function (
        declare,
        _WidgetBase,
        _TemplatedMixin,
        template,
        i18n,
        FeatureLayer,
        Query
    ) {
        return declare([_WidgetBase, _TemplatedMixin], {
            templateString: template,
            i18n: i18n,
            startup: function () {
                this.inherited(arguments);
                
                this.initForm();
                this.fillDataTable();

            },
            initForm: function () {

                $('#date_example').datepicker({
                    // endDate: new Date(),
                    // startDate: new Date()
                });

                this.fillSelect();
                this.setValidator();

            },
            fillSelect: function () {
                
                var layer = new FeatureLayer('http://10.6.99.187:6080/arcgis/rest/services/OufokMapSevices/D%C3%A9coupage_SDA/MapServer/6', {
                    mode: FeatureLayer.MODE_ONDEMAND,
                    outFields: ["*"]
                });

                var query = new Query();
                query.where = "1=1";
                var data = [];

                layer.queryFeatures(query, function (response) {
                        for (let i = 0; i < response.features.length; i++) {
                            data.push({
                                id: response.features[i].attributes.nom_commune,
                                text: response.features[i].attributes.nom_commune
                            });
                        }
                        $('#select_commune').select2({
                            width: '100%',
                            placeholder: "Séléctionner une commune ...",
                            data: data,
                            allowClear: true
                        });

                    },
                    function (error) {
                        console.log(error);
                    });

            },
            setValidator: function () {
                var options = {
                    // Specify validation rules
                    excluded: ':disabled, :hidden, :not(:visible), [aria-hidden]',
                    rules: {
                        textbox_1: "required",
                        textbox_2: "required",
                        textbox_3: {
                            required: true,
                            number:true,
                            maxlength: 2
                        }
                    },
                    // Specify validation error messages
                    messages: {
                        textbox_1:i18n.requiredMessage,
                        textbox_2: i18n.requiredMessage,
                        textbox_3: {
                            required: i18n.requiredMessage,
                            number:i18n.validNumber,
                            maxlength: i18n.maxValue
                        },

                    }
                }

                $("#formID").validate(options);

                $(this.myButton).on("click", function(){                                        
                    if ($("#formID").valid()) {
                        alert("Valid form");                    }
                });
               
            },
            fillDataTable: function () {
                var dataSet = [{
                        "first_name": "Airi",
                        "last_name": "Satou",
                        "position": "Accountant",
                        "office": "Tokyo",
                        "start_date": "28th Nov 08",
                        "salary": "$162,700"
                    },
                    {
                        "first_name": "Angelica",
                        "last_name": "Ramos",
                        "position": "Chief Executive Officer (CEO)",
                        "office": "London",
                        "start_date": "9th Oct 09",
                        "salary": "$1,200,000"
                    },
                    {
                        "first_name": "Ashton",
                        "last_name": "Cox",
                        "position": "Junior Technical Author",
                        "office": "San Francisco",
                        "start_date": "12th Jan 09",
                        "salary": "$86,000"
                    },
                    {
                        "first_name": "Bradley",
                        "last_name": "Greer",
                        "position": "Software Engineer",
                        "office": "London",
                        "start_date": "13th Oct 12",
                        "salary": "$132,000"
                    }

                ];


                var table = $('#example').DataTable({
                    data: dataSet,
                    responsive: true,
                    columns: [{
                            title: "first name",
                            data: "first_name"
                        },
                        {
                            title: "last name",
                            data: "last_name"
                        },
                        {
                            title: "position",
                            data: "position"
                        },

                        {
                            "render": function (data, type, row, meta) {
                                return `<div class="text-center">
                                    <button class="btn btn-outline-secondary btn-sm" data-action="edit"><i class="far fa-edit"></i></button>
                                    <button class="btn btn-outline-secondary btn-sm" data-action="delete"><i class="far fa-trash-alt"></i></button>
                                    </div>`;
                            }
                        }

                    ]
                });

                $('#example').on('click', '[data-action="edit"]', function (e) {
                    console.log(table.row($(this).parents('tr')).data()["salary"]);
                    alert("edit");
                });
                $('#example').on('click', '[data-action="delete"]', function (e) {
                    console.log(table.row($(this).parents('tr')).data()["first_name"]);
                    alert("delete");
                });
            }


        });
    });