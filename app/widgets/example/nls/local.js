define({
    root: {
        simpleExample: "Exemple simple",
        submit:"Valider",
        cancel:"Annuler",
        requiredMessage:"Ce champ est obligatoire",
        validNumber:"Veuillez, entrer un nombre valide",
        maxValue:"Veuillez entrer au plus 2 caractères"
    },
    "ar": true
});