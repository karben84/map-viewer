define({
    root: {
        drawTitle:"Exemple de dessin, d'édition et de suppression avec interaction avec la base de données",
        drawLine: "Dessiner ligne",
        editLine: "Editer ligne",
        deleteLine: "Supprimer ligne"
    },
    "ar": true
});