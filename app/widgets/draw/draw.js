define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/text!app/widgets/draw/draw.html",
    "dojo/i18n!app/widgets/draw/nls/local",
    "esri/toolbars/draw",
    "esri/toolbars/edit",
    "esri/graphic",
    "esri/tasks/query",
    "esri/layers/FeatureLayer",
    "esri/symbols/SimpleMarkerSymbol",
    "js/events",
    "dojo/domReady!"

],
function (
    declare,
    _WidgetBase,
    _TemplatedMixin,
    template,
    i18n,
    Draw,
    Edit,
    Graphic,
    Query,
    FeatureLayer,
    SimpleMarkerSymbol,
    events

) {
    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        i18n: i18n,
        editedGraphic: null,

        startup: function () {
            this.inherited(arguments);
            var toolbar = new Draw(this.map, {
                tooltipOffset: 20,
                drawTime: 90
            });

            var editToolbar = new Edit(this.map, );

            var layer = this.map.getLayer("line");

            var tool = 0;
            // tool = tool | Edit.MOVE;
            tool = tool | Edit.EDIT_VERTICES;
            // tool = tool | Edit.SCALE;
            // tool = tool | Edit.ROTATE;

            toolbar.on("draw-end", this.saveFeature);
            events.addEvent("toolbar2", toolbar);

            $('#btn-draw-line2').click($.proxy(function (e) {
                events.clearAllEvents();
                toolbar.activate(Draw.POLYLINE);
            }, this));

            $('#btn-edit-line2').click($.proxy(function (e) {

                events.clearAllEvents();

                var mapClick = this.map.on("click", $.proxy(function (evt) {

                    if (evt.graphic) {
                        this.editedGraphic = evt.graphic;
                        editToolbar.activate(Edit.EDIT_VERTICES, evt.graphic);

                        console.log("click on graphic");

                    } else {

                        console.log("click out side graphic");

                        if (this.editedGraphic) {
                            editToolbar.deactivate();
                        }

                    }
                }, this));

                events.addEvent("mapClick", mapClick);

                editToolbar.on("deactivate", function (evt) {

                    query = new Query();
                    query.where = "objectid = " + evt.graphic.attributes.objectid;

                    layer.selectFeatures(query, FeatureLayer.SELECTION_NEW, $.proxy(function (features) {

                        layer.applyEdits(null, [features[0]], null);

                    }), this);
                })

            }, this));

            $('#btn-delete-line2').click($.proxy(function (e) {
                events.clearAllEvents();

                var layerClick = layer.on("click", function (evt) {

                    query = new Query();
                    query.where = "objectid = " + evt.graphic.attributes.objectid;

                    layer.selectFeatures(query, FeatureLayer.SELECTION_NEW, $.proxy(function (features) {

                        layer.applyEdits(null, null, [features[0]]);

                    }), this);
                });

                events.addEvent("layerClick", layerClick);

            }, this));

        },
        saveFeature: function (evt) {

            var graphic = new Graphic(evt.geometry);

            var layer = this.map.getLayer("line");

            layer.applyEdits([graphic], null, null);
            events.clearAllEvents();
            events.initEvent();
        }


    });
});